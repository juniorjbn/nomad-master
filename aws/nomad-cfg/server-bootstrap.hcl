bind_addr = "192.168.0.8"

advertise {
  rpc = "192.168.0.8:4647"
}
log_level = "DEBUG"
data_dir = "/opt/nomad/data"

server {
    enabled = true
    bootstrap_expect = 3
}